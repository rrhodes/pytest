from json import loads

from pytest import fixture, mark, raises

from src.app import fetch_payload_event_type, validate_payload

TEST_RESOURCES_PATH = "./tests/resources/{}.json"


@fixture
def invalid_payload():
    with open(TEST_RESOURCES_PATH.format("invalid_payload"), "r") as f:
        return loads(f.read())


@mark.parametrize(("payload_name", "expected_result"),
                  [("invalid_payload", False), ("valid_payload", True)],
                  ids=["invalid_payload", "valid_payload"])
def test_validate_payload(payload_name, expected_result):
    with open(TEST_RESOURCES_PATH.format(payload_name), "r") as f:
        payload = loads(f.read())

    actual_result = validate_payload(payload)

    assert expected_result == actual_result


def test_fetch_payload_event_type_exception(invalid_payload):
    with raises(KeyError):
        fetch_payload_event_type(invalid_payload)
