import logging

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.ERROR)


def validate_payload(payload: dict) -> bool:
    """
    Validate that the payload contains an 'eventType' key
    :param payload: Python dictionary payload to be validated
    :return: True if 'eventType' key exists in payload, otherwise False
    """
    LOGGER.info("Validating payload")
    return "eventType" in payload


def fetch_payload_event_type(payload: dict) -> str:
    """
    Retrieve the event type from the payload
    :param payload: Payload to retrieve an event type from
    :return: The event type of the payload
    :raise: KeyError if the 'eventType' key does not exist
    """
    LOGGER.info("Fetching event type from the payload")
    try:
        return payload["eventType"]
    except KeyError as err:
        LOGGER.error(err)
        raise err
